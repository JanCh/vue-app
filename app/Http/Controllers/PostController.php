<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use Inertia\Inertia;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::select()
            ->with('user:id,name')
            ->orderBy('created_at', 'DESC');
        
        if (!Auth::check()) {
            $posts = $posts->where('is_public', true);
        }
        
        $posts = $posts->paginate(15);

        return Inertia::render('Dashboard', [
            'status' => session('status'),
            'posts' => $posts,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PostRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PostRequest $request): RedirectResponse
    {
        $validated = $request->validated();

        $post = new Post($validated);
        $post->user()->associate(Auth::user());

        $post->save();

        return back()->with('status', 'Added');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\PostRequest  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(PostRequest $request, Post $post): RedirectResponse
    {
        $validated = $request->validated();

        $post->fill($validated);
        $post->save();

        return back()->with('status', 'Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Post $post): RedirectResponse
    {
        if (Auth::user()->cannot('update', $post)) {
            return back()->withErrors(['cannot' => 'Hey you!']);
        }

        $post->delete();

        return back()->with('status', 'Deleted');
    }
}
