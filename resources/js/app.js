require('./bootstrap');

// Import modules...
import { createApp, h } from 'vue';
import { createInertiaApp } from '@inertiajs/inertia-vue3';
import { InertiaProgress } from '@inertiajs/progress';

const el = document.getElementById('app');

createInertiaApp({
    resolve: (name) => require(`./Pages/${name}`),
    setup({ el, app, props, plugin }) {
        createApp({ render: () => h(app, props) })
            .mixin({ 
                methods: { 
                    route,
                    /**
                    * Translate the given key.
                    */
                    __(key, replace = {}) {
                        console.log(this)
                        var translation = this.$page.language[key]
                            ? this.$page.language[key]
                            : key
                
                        Object.keys(replace).forEach(function (key) {
                            translation = translation.replace(':' + key, replace[key])
                        });
                
                        return translation
                    },
                
                    /** 
                     * Translate the given key with basic pluralization. 
                     */
                    __n(key, number, replace = {}) { 
                        var options = key.split('|');   
                
                        key = options[1]; 
                        if(number == 1) { 
                            key = options[0]; 
                        }   
                
                        return tt(key, replace); 
                    },
                },
            })
            .use(plugin)
            .mount(el);
    },
});

InertiaProgress.init({ color: '#4B5563' });
